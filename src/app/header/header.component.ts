import { Component, OnInit } from '@angular/core';
import { MdDialog, MdDialogRef} from '@angular/material';
import {LoginComponent} from "../login/login.component";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public dailog: MdDialog) { }

  ngOnInit() {
  }
  openLoginForm(){
    this.dailog.open(LoginComponent, {width:'400px' , height: '350px'});
  }
}
