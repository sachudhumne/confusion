import { Injectable } from '@angular/core';
import {Dish} from "../shared/dish";
import {Observable} from "rxjs/Observable";
import 'rxjs';
import {Http} from "@angular/http";
import {ProcessHTTPMsgService} from "./process-httpmsg.service";
import {baseURL} from "../shared/baseurl";
import {DISHES} from "../shared/dishes";

@Injectable()
export class DishService {

  constructor(private http: Http, private processHTTPMsgService: ProcessHTTPMsgService) { }

  getDishes(): Observable<Dish[]> {
    //return this.http.get(baseURL +'dishes');
    return Observable.of(DISHES).delay(1000);
  }

  getDish(id: number): Observable<Dish> {
    return Observable.of(DISHES.filter((dish) => (dish.id === id))[0]).delay(2000);
  }

  getFeaturedDish(): Observable<Dish> {
    return Observable.of(DISHES.filter((dish) => (dish.featured === true))[0]).delay(2000);
  }

  getDishIds(): Observable<number[]> {
    return Observable.of(DISHES.map(dish => dish.id));
  }
}
