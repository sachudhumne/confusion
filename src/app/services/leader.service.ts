import { Injectable } from '@angular/core';
import {Leader} from "../shared/Leader";
import {LEADERS} from "../shared/Leaders";

@Injectable()
export class LeaderService {
  constructor() { }

  getLeaders(): Promise<Leader[]>{
    return Promise.resolve(LEADERS);
  }
  getLeader(id: number): Promise<Leader>{
    return Promise.resolve(LEADERS.filter((lead)=>(lead.id === id))[0]);
  }
  getFeaturedLeader(): Promise<Leader> {
    return Promise.resolve(LEADERS.filter((lead) => (lead.featured === true))[0]);
  }
}
