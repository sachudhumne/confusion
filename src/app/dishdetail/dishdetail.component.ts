import {Component, OnInit} from '@angular/core';
import { Dish } from '../shared/dish';
import {DishService} from "../services/dish.service";
import {DISHES} from '../shared/dishes';
import {ActivatedRoute, Params} from "@angular/router";
import {Location} from '@angular/common';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { Comment } from '../shared/comment';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss']
})
export class DishdetailComponent implements OnInit {
  dish: Dish;
  dishIds: number[];
  prev: number;
  next: number;
  commentForm: FormGroup;
  latestComment = {
    author: '',
    rating: 0,
    comment: '',
    date: ''
  };
  formErrors = {
    'author': '',
    'rating': 0,
    'comment': ''
  };
  validationMessage = {
    'author': {
      'required': 'Name is Required',
      'minlength': 'Name must be at least 2 characters long.',
      'maxlength': 'Name cannot be more than 25 characters long.'
    },
    'rating': {
      'required': 'Rating is Required',
    },
    'comment': {
      'required': 'Comment is Required',
      'minlength': 'Comment must be at least 2 characters long.',
      'maxlength': 'Comment cannot be more than 300 characters long.'
    }
  };
  constructor(private dishService: DishService, private route: ActivatedRoute, private location: Location,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.dishService.getDishIds().subscribe(dishIds => this.dishIds = dishIds);
    this.route.params
      .switchMap((params: Params) => this.dishService.getDish(+params['id']))
      .subscribe(dish => {this.dish = dish, this.setPervNext(dish.id)});
    this.createCommentForm();
  }

  createCommentForm() {
    this.commentForm = this.fb.group({
      author: ['',[Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      rating: [5,[Validators.required, Validators.min(1), Validators.max(5)]],
      comment: ['',[Validators.required,Validators.minLength(2), Validators.maxLength(300)]],
      date: ['',]
    });
    this.commentForm.valueChanges
      .subscribe((data) => {
        this.onValueChanged(data);
        this.latestComment = this.commentForm.value;
      });
    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    if(!this.commentForm){
      return;
    }
    const form = this.commentForm;
    for(let field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if(control && control.dirty && control.invalid) {
        const messages = this.validationMessage[field];
        for(let key in control.errors){
          this.formErrors[field] = messages[key] + ' ';
        }
      }
    }
  }

  onSubmit(){
    this.latestComment.date = new Date().toISOString();
    this.dish.comments.push(this.latestComment);
    this.commentForm.reset({
      author: '',
      rating: 5,
      comment: ''
    });
  }

  setPervNext(dishId: number) {
    let index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1)%this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1)%this.dishIds.length];
  }
  goBack(): void {
    this.location.back();
  }
}
