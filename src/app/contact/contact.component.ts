import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validator, Validators} from "@angular/forms";
import {Feedback, contactTypes} from '../shared/feedback';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  feedback: Feedback;
  feedbackForm: FormGroup;
  contactType = contactTypes;

  formErrors = {
    'firstName': '',
    'lastName': '',
    'telNum': '',
    'email': ''
  };

  validationMessages = {
    'firstName': {
      'required': 'First Name is Required',
      'minlength': 'First Name must be at least 2 characters long.',
      'maxlength': 'FirstName cannot be more than 25 characters long.'
    },
    'lastName': {
      'required': 'Last Name is required.',
      'minlength': 'Last Name must be at least 2 characters long.',
      'maxlength': 'Last Name cannot be more than 25 characters long.'
    },
    'telNum': {
      'required': 'Tel. number is required.',
      'pattern': 'Tel. number must contain only numbers.'
    },
    'email': {
      'required': 'Email is required.',
      'email': 'Email not in valid format.'
    },
  }
  constructor(private fb: FormBuilder) {
    this.createForm();
  }
  ngOnInit() {
  }
  createForm() {
    this.feedbackForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.maxLength(25), Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.maxLength(25), Validators.minLength(2)]],
      telNum: [0, [Validators.required, Validators.pattern]],
      email: ['', [Validators.required, Validators.email]],
      agree: false,
      contactType: 'None',
      message: ''
    });
    this.feedbackForm.valueChanges
      .subscribe((data)=> this.onValueChanged(data));
    this.onValueChanged(); // reset the validation
  }
  onValueChanged(data? : any) {
    if (!this.feedbackForm){
      return ;
    }
    const form = this.feedbackForm;
    for (const field in this.formErrors){
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.invalid && control.dirty) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] +' ';
        }
      }
    }
  }
  onSubmit() {
    this.feedback = this.feedbackForm.value;
    console.log(this.feedback);
    this.feedbackForm.reset();
  }
}
