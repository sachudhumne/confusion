import {Component, Input, OnInit} from '@angular/core';
import {User} from "../shared/user";
import {MdDialogRef} from "@angular/material";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user= {remember: false};
  constructor(public mdDialogRef: MdDialogRef<LoginComponent>) { }

  ngOnInit() {
  }
  onSubmit() {
    this.mdDialogRef.close();
  }
}
