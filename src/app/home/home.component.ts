import { Component, OnInit } from '@angular/core';
import {DishService} from "../services/dish.service";
import {PromotionService} from "../services/promotion.service";
import {Dish} from "../shared/dish";
import {Promotion} from "../shared/promotion";
import {Leader} from "../shared/Leader";
import {LeaderService} from "../services/leader.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  dish: Dish;
  promotion: Promotion;
  leader: Leader;

  constructor(private dishService: DishService, private promoService: PromotionService, private leaderService: LeaderService) {
  }

  ngOnInit() {
    this.dishService.getFeaturedDish().subscribe(dish=> this.dish = dish);
    this.promotion = this.promoService.getFeaturedPromotion();
    this.leaderService.getFeaturedLeader().then(leader => (this.leader = leader));
  }
}
