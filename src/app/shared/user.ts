/**
 * Created by sachin on 16-Dec-17.
 */
export class User {
  username: string;
  password: string;
  remember: boolean;
}
