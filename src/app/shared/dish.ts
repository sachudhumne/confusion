/**
 * Created by sachin on 02-Dec-17.
 */
import { Comment } from '../shared/comment';

export class Dish {
  id: number;
  featured: boolean;
  name: string;
  image: string;
  category: string;
  label: string;
  price: string;
  description: string;
  comments: Comment[];
}
