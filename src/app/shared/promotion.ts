/**
 * Created by sachin on 02-Dec-17.
 */
export class Promotion {
  id: number;
  name: string;
  image: string;
  label: string;
  price: string;
  featured: boolean;
  description: string;
}
