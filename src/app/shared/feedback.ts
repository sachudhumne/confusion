/**
 * Created by sachin on 19-Dec-17.
 */
export  class Feedback {
  firstName: string;
  lastName: string;
  telNum: number;
  email: string;
  agree: boolean;
  contactType: string;
  message: string;
}

export const contactTypes = ['None','TelNum','Email'];
