/**
 * Created by sachin on 02-Dec-17.
 */
export class Comment {
  rating: number;
  comment: string;
  author: string;
  date: string;
}
